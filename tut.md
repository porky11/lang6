Before starting the tutorial have a look at the documentation.
When you want to see a list of defined words, look into the `.x` file, named by the word type.

# Grammar

A sentence is described by a single object. The object mentioned in a sentence is assumed to exist. Every sentence ends with a noun.

## Basic

Basic objects are nouns representing very abstract objects.

* `i`

=> "I (exist)"; always true, except for non-personal descriptions

* `u`

=> "you (exist)"; only true, when talking to someone

* `a`

=> "something (exists)"; always true

## Unions

Simple unions are modifiers, used to create simple unions with basic objects.

They are written behind nouns.

It's recommended to write modifiers directly behind the modified word without seperation. You can use `-` to seperate the modified word from the modifier. If no `-` is used, as many consonants as possible belong to the modifier.

* `iju` (or `i-ju`, not `ij-u`)

=> "we" (I and you)

* `ija`

=> "we" (I and someone)

* `ijuja`

=> "we all" (I, you and someone)

* `uja`

=> "you" (you and someone)

## Properties

Properties are specifiers, used to describe an object more accurately.

When written before some object, the meaning of the following object is specified.

Simple adjectives just represent generic properties, which cannot be specified further.

* `hu a`

=> "person" (someone, who is a person)

* `hu i`

=> "I'm a person" (I exist, who is a person)

* `ma a`

=> "male"

* `la a`

=> "child"

* `ma la hu a`

=> "boy" (someone, who is a person, who is a child, who is male)

## Selectors

Basic selectors are specifiers and specify, which and how many objects are talked about.

* `ij a`

=> "a thing"

* `ij hu a`

=> "a person"

* `il hu a`

=> "the person (mentioned before)"

* `al a`

=> "everything" (all things)

* `ul a`

=> "what?" (which thing)

* `uj a`

=> "nothing" (no thing)

* `aj a`

=> "many things"

* `av a`

=> "a few things" (some things)

* `ul fi zu a`

=> "which woman?"

* `uv fi a`

=> "that woman"; answer

* `fi ul a`

=> "who is female?"

* `uv i`

=> "I am" (i, who is the object asked about, exists)

* `iv ma ul la u`

=> "Who of you children is that boy (I point to)?" (you, who are children, who I ask about, who are the male, I point to); talking to multiple persons

* `uj u`

=> "Noone of you exists"; only true when not talking to anyone

* `uj la i`

=> "I'm no child" (I, who is a child, does not exist)

## Specifier Modifiers

Specifier modifers are modifiers used for modifying meanings of single words. The resulting word is a specifier.

* `lakajn i`

=> "I'm not a child" (I, who is not a child, exists)

* `hu makajn u`

=> "You are a person, who is not male"

Most modifiers are more useful with a different kind of specifiers.

## Values

Values are specifiers used to describe, that an object has a property. This property can have multiple values, which can be used using modifers.

* `svil i`

=> "I have a velocity"

* `svilpiz i`

=> "I am slow"

* `svilpaz ma la hu i`

=> "I'm a fast boy"; fast for being a boy

* `nujkajn u`

=> "You are sexless"/"You don't have a gender"

* `svilkajn a`

=> "Something doesn't have a velocity"

* `sajpu hu a`

=> "An avarage sized person"

* `sajpuj hu a`

=> "Something having the same size as persons"

* `sajpa hu iju`

=> "We are tall humans"

* `sajpaj iju`

=> "There exist things larger than us"

## Absolute Modifiers

Object modifers are modifiers, just like basic modifier specifiers, but the resulting word is an object.

* `sajpulz a`

=> "Size"

* `sajpuz a`

=> "Avarage sizes"

* `svalpij ij sajpiz a`

=> "something smaller than a small size"

## Actions

Actions can be described in different ways, but some common actions are described by particles and cannot be specified further.

* `iln u i`

=> "I like you"

* `ajf an milpa a i i`

=> "I make me move at a beautiful place"; "I walk around at a nice place"

* `ajf anvaj ij ma a anvuj ij fi a av lalkilfpa la a u`

=> "The mover from a male to a female of some very cute children is you"; "You take some very cute children from a man and give them to a woman"

