# Phonology

## Vowels

* a: /a/
* i: /i/
* u: /u/

## Approximants

* v: /w/
* j: /j/

## L

* l: /l/

## Nasal

* m: /m/
* n: /n/

## Fricative

* f: /f/
* s: /s/
* z: /ʃ/
* h: /x/

## Stops

* p: /p/
* t: /t/
* k: /k/

# Phonotactics

- `(` ABC `)`: sequence "ABC" content optional
- `[` ABC `]`: sequence "ABC" grouped
- ABC `|` DEF: sequence "ABC" alternative to sequence "DEF"

To simplify the phonotactics, some groups will be united into new groups:

- `M` => `N|L`
- `E` => `A|L`
- `C` => `F|N`

So the phonotactics look like this:

`(S)(F)(M)(A)V(E)(C)`

# Grammar

Words can be seperated into three categories, called word kinds:

* Objects
* Specifiers
* Particles

These word kinds only have grammatical differences.

An object ends a sentence and represents that object.

Specifiers are written before objects to specify more about this object.

Particles work like specifiers, but the object can be an entire sentence.

For each word kind there can also exist modifiers. A modifier changes the meaning of the preceding word and acts as a replacement for this word.
So if multiple modifiers appear in a row, the modified word is modified.

## Objects

* Basic: `V`
* Unions: `AV` (mod)
* Numbers: `AVC` (mod)

## Specifiers

* Modifiers: `VE`
* Properties: `[FM|F|M](A)V`
* Values: `[FM|F|M](A)VE`
* Nouns: `[FM|F|M](A)V(E)C`
* Absolute Modifiers: `SV(E)C` (mod)

## Particles

* Prepositions: `VC`
* Actions: `VEC`
* Relations: `AVE`
* Relative Objects: `[FM|F|M](A)VC`
* Relative Modifiers: `SV(E)` (mod)

## Unknown

- `SV(E)(C)`
- `SAV(E)`
- `SAVC`
- `SAVEC`
- `S[FM|F|M](A)V(E)(C)`

